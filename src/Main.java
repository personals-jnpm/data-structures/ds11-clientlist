import controllers.StoreController;
import models.Client;
import utiliy.Keyboard;

public class Main {

    public static Keyboard keyboard = new Keyboard();
    public static StoreController storeController = new StoreController();

    public static void main(String[] args) {
        Main main = new Main();

        main.menu();
        int option = -1;
        do {
            try {
                System.out.print("\nSeleccione una opción en el menú principal: ");
                option = keyboard.readIntegerDefault(-1);
                switch (option) {
                    case 0 -> System.out.println(" El programa ha finalizado");
                    case 1 -> main.addClient();
                    case 2 -> main.printClients();
                    case 3 -> main.searchClient();
                    case 4 -> main.updateClient();
                    case 5 -> main.deleteClient();
                    default -> System.out.println(" ¡Opción no disponible en el menú principal!");
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        } while (option != 0);
    }

    public void menu() {
        System.out.println("╔════════════════════════════════════════════════════════╗");
        System.out.println("╠----------------------" + storeController.getBasicInformation().get(0) + "---------------------╣");
        System.out.println("║════════════════════════════════════════════════════════║");
        System.out.println("║   Address: " + storeController.getBasicInformation().get(1) + "                               ║");
        System.out.println("║   NIT: " + storeController.getBasicInformation().get(2) + "                                   ║");
        System.out.println("║════════════════════════════════════════════════════════║");
        System.out.println("║   1. Agregar cliente a la tienda                       ║");
        System.out.println("║   2. Mostrar clientes                                  ║");
        System.out.println("║   3. Buscar cliente                                    ║");
        System.out.println("║   4. Actualizar cliente                                ║");
        System.out.println("║   5. Eliminar cliente                                  ║");
        System.out.println("║   0. Salir                                             ║");
        System.out.println("╚════════════════════════════════════════════════════════╝");
    }

    public void addClient() {
        System.out.println(" Crear cliente:");
        storeController.addClient(registerClient());
    }

    public Client registerClient() {
        Client client = new Client();

        System.out.print("  Ingrese el nombre del cliente: ");
        client.setClientName(keyboard.readLine());
        System.out.print("  Ingrese el número de documento del cliente: ");
        client.setIdNumber(keyboard.readLine());
        System.out.print("  Ingrese el email del cliente: ");
        client.setEmail(keyboard.readLine());
        System.out.print("  Ingrese el número de teléfono del cliente: ");
        client.setPhone(keyboard.readLine());

        return client;
    }

    public void printClients() {
        System.out.print(" ");
        for (Client client : storeController.findAllClients()) {
            System.out.println(client);
        }
    }

    public void searchClient() throws Exception {
        System.out.println(storeController.findClientById(keyboard
                .readValidPositiveInteger(" Buscar cliente:\n  Ingrese el id del cliente: ")).toString());
    }

    public void updateClient() throws Exception {
        storeController.updateClient(keyboard
                .readValidPositiveInteger(" Actualizar cliente:\n  Ingrese el id del cliente: "), registerClient());
    }

    public void deleteClient() throws Exception {
        storeController.deleteClient(keyboard.
                readValidPositiveInteger(" Eliminar cliente:\n  Ingrese el id del cliente: "));
    }

}
