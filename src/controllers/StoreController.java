package controllers;

import models.Client;
import models.Store;

import java.util.ArrayList;
import java.util.List;

public class StoreController {

    private final Store store;

    public StoreController() {
        store = new Store();
        initBasicInformation();
    }

    public void addClient(Client client) {
        store.addClient(client);
    }

    public void updateClient(int id, Client client) throws Exception {
        store.updateClient(id, client);
    }

    public void deleteClient(int id) throws Exception {
        store.delete(id);
    }

    public List<Client> findAllClients() {
        return store.findAll();
    }

    public Client findClientById(int id) throws Exception {
        return store.findById(id);
    }

    public List<String> getBasicInformation(){
        List<String> basicInfo = new ArrayList<>();

        basicInfo.add(store.getNameShop());
        basicInfo.add(store.getAddress());
        basicInfo.add(store.getNIT());

        return basicInfo;
    }

    public void initBasicInformation(){
        store.setAddress("Cra 3 N° 5-23");
        store.setNameShop("Tiendas IKENA");
        store.setNIT("1235-452-1235");
    }

}
