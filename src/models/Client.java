package models;

public class Client {

    private int clientId;
    private String clientName;
    private String idNumber;
    private String email;
    private String phone;

    public Client(int clientId, String clientName, String idNumber, String email, String phone) {
        this.clientId = clientId;
        this.clientName = clientName;
        this.idNumber = idNumber;
        this.email = email;
        this.phone = phone;
    }

    public Client() {
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return  "\nCliente [" + clientId + "]{" +
                "\n Nombre: " + clientName +
                "\n Cédula: " + idNumber +
                "\n Correo: " + email +
                "\n Teléfono: " + phone +
                "\n}";
    }
}
