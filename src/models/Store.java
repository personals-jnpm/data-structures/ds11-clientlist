package models;

import java.util.ArrayList;
import java.util.List;

public class Store {

    private List<Client> clients;
    private String nameShop;
    private String NIT;
    private String address;

    public Store() {
        clients = new ArrayList<>();
        initClients();
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public String getNameShop() {
        return nameShop;
    }

    public void setNameShop(String nameShop) {
        this.nameShop = nameShop;
    }

    public String getNIT() {
        return NIT;
    }

    public void setNIT(String NIT) {
        this.NIT = NIT;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    // Métodos del listado de los clientes
    public void addClient(Client client){
        client.setClientId(clients.size());
        clients.add(client);
    }

    public List<Client> findAll() {
        return clients;
    }

    public void updateClient(int id, Client game) throws Exception {
        validIndex(id);
        game.setClientId(id);
        clients.set(id, game);
    }

    public void delete(int id) throws Exception {
        validIndex(id);
        clients.remove(id);
    }

    public Client findById(int id) throws Exception {
        validIndex(id);
        return clients.get(id);
    }

    private void validIndex(int id) throws Exception {
        if (clients.size() <= id || id < 0) {
            throw new Exception("  Error: El id del cliente no es válido, intenta nuevamente");
        }
    }

    public void initClients() {
        clients.add(new Client(0, "Nicolás Pérez", "1002416680", "jaider@gmail.com",
                "3124849334"));
        clients.add(new Client(1, "María Montaño", "1003542698", "maria@gmail.com",
                "3115248693"));
        clients.add(new Client(2, "Carlos Perea", "1005125486", "carlos@gmail.com",
                "3154268531"));
        clients.add(new Client(3, "Camila Cifuentes", "23256421", "camila@gmail.com",
                "3124851329"));
        clients.add(new Client(4, "Pedro Jaramillo", "4325987", "pedro@gmail.com",
                "3118524639"));
    }

    @Override
    public String toString() {
        return "Store{" +
                "clients=" + clients +
                ", nameShop='" + nameShop + '\'' +
                ", NIT='" + NIT + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
